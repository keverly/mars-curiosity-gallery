//
//  ImageDetailVC.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/23/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import UIKit

class ImageDetailVC: UIViewController {

    var viewModel: ImageDetailVM!
    var roverImageView = UIImageView(frame: .zero)
    var infoTableView = UITableView(frame: .zero, style: .grouped)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        viewModel.fetchPhoto() { [weak self] data in
            guard let self = self else { return }
            guard let data = data else { return }
            DispatchQueue.main.async {
                self.roverImageView.image = UIImage(data: data)
            }
        }
    }
    
    func setupViews() {
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = UIColor.systemBackground
        } else {
            self.view.backgroundColor = .white
        }
        
        roverImageView.translatesAutoresizingMaskIntoConstraints = false
        roverImageView.contentMode = .scaleAspectFit
        self.view.addSubview(roverImageView)
        
        var constraints = [
            roverImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            roverImageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            roverImageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            roverImageView.heightAnchor.constraint(equalToConstant: 300)
        ]
        
        infoTableView.register(PhotoInfoCell.self, forCellReuseIdentifier: PhotoInfoCell.stringClass)
        infoTableView.translatesAutoresizingMaskIntoConstraints = false
        infoTableView.delegate = self
        infoTableView.dataSource = self
        self.view.addSubview(infoTableView)
        
        constraints += [
            infoTableView.topAnchor.constraint(equalTo: self.roverImageView.bottomAnchor),
            infoTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            infoTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            infoTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}


extension ImageDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return viewModel.roverPhotoInfo.count
        case 1:
            return viewModel.roverPhotoCamera.count
        case 2:
            return viewModel.roverPhotoRover.count
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PhotoInfoCell.stringClass) as! PhotoInfoCell
        var titleText = ""
        var detailText = ""
        
        switch indexPath.section {
        case 0:
            titleText = viewModel.roverPhotoInfo[indexPath.row].0
            detailText = viewModel.roverPhotoInfo[indexPath.row].1
        case 1:
            titleText = viewModel.roverPhotoCamera[indexPath.row].0
            detailText = viewModel.roverPhotoCamera[indexPath.row].1
        case 2:
            titleText = viewModel.roverPhotoRover[indexPath.row].0
            detailText = viewModel.roverPhotoRover[indexPath.row].1
        default:
            break
        }
        
        cell.setUp(title: titleText, detail: detailText)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Info"
        case 1:
            return "Camera"
        case 2:
            return "Rover"
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}
