//
//  MainGalleryVC.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/17/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import UIKit

class MainGalleryVC: UIViewController {

    var roverImageColView: UICollectionView!
    var viewModel: MainGalleryVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = MainGalleryVM(delegate: self)
        
        // Setup Collection View
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: 150, height: 150)
        
        roverImageColView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        roverImageColView.delegate = self
        roverImageColView.dataSource = self
        roverImageColView.register(RoverImageCell.self, forCellWithReuseIdentifier: RoverImageCell.stringClass)
        
        self.view.addSubview(roverImageColView)
        
        // Load the manifest (So we know the total number of rows and sections)
        viewModel.fetchManifest()
    }
    
    func presentImageDetail(selectedPhoto: RoverPhoto) {
        let newVC = ImageDetailVC()
        newVC.modalTransitionStyle = .coverVertical
        newVC.modalPresentationStyle = .overFullScreen
        let imageDetailVM = ImageDetailVM(selectedPhoto)
        newVC.viewModel = imageDetailVM
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        roverImageColView.reloadData()
    }
}

// MARK: - COLLECTION DELEGATE
extension MainGalleryVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.totalPhotosFor(sol: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RoverImageCell.stringClass, for: indexPath) as! RoverImageCell
        
        // Find the Photo url that we need then tell
        let sol = indexPath.section
        let page = (indexPath.row / NasaAPI.maxItemsPerPage) + 1
        let offset = (indexPath.row % NasaAPI.maxItemsPerPage)
        
        
        // Tell the viewModel to get the URL and download the image
        self.viewModel.fetchPhotoFor(sol: sol, page: page, offset: offset) { (data) in
            guard let data = data else { return }
            
            // Do the loading of image data in the background
            DispatchQueue.global(qos: .background).async {
            // Once we have the data for the image. Update the Cell on the main thread
                let image =  UIImage(data: data)
                DispatchQueue.main.async {
                    cell.image = image
                    cell.index = indexPath.row
                }
            }
        }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.totalPhotos
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // We've used this logic a couple of times, we can probably consolidate somewhere.
        let sol = indexPath.section
        let page = (indexPath.row / NasaAPI.maxItemsPerPage) + 1
        let offset = (indexPath.row % NasaAPI.maxItemsPerPage)
        guard let roverPhoto = viewModel.photoFor(sol: sol, page: page, offset: offset) else { return }
                
        self.presentImageDetail(selectedPhoto: roverPhoto)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let page = (indexPath.row / NasaAPI.maxItemsPerPage) + 1
        if !viewModel.hasLoadedPage(sol: indexPath.section, page: page) {
            viewModel.fetchPage(sol: indexPath.section, page: page)
        }
    }
}


// MARK: - VM DELEGATE
extension MainGalleryVC: MainGalleryVMDelegate {
    func onFetchPageCompleted(with newIndexPathsToReload: [IndexPath]?) {
        // This line is what is causing some flickering while scrolling.
        // It is required though to ensure all cells get their images.
        // Find a way to make this smoother!
        self.roverImageColView.reloadItems(at: newIndexPathsToReload ?? [])
    }
    
    func onFetchManifestCompleted() {
        self.roverImageColView.reloadData()
    }
}
