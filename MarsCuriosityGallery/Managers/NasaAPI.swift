//
//  NasaApiManager.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/17/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import Foundation
import Alamofire


struct NasaAPI {
    
    struct MissingAPIKeyError: Error {
        var message = "No API Key available"
    }

    // Properties we will use for all requests
    static let urlBase = "https://api.nasa.gov"
    static let apiKey = Bundle.main.infoForKey("NASA_API_KEY")
    static let maxItemsPerPage = 25
    
    // MARK: - REQUESTS
    /// Get's the manifest informaiton for the curiosity rover
    /// - Parameter completion: Callback for when request completes
    static func getManifest(completion: @escaping (Result<Data>) -> Void) -> DataRequest? {
        guard let apiKey = self.apiKey else {
            completion(.failure(MissingAPIKeyError()))
            return nil
        }
        
        // Setup request
        let url = self.urlBase + "/mars-photos/api/v1/manifests/curiosity"
        let params: Parameters = ["api_key": apiKey]
        let request = Alamofire.request(url, parameters: params)
        
        // Execute request
        request.validate().responseData { (response) in
            switch response.result {
            case .success(let value):
                completion(.success(value))

            case .failure(let error):
                completion(.failure(error))
            }
        }
        return request
    }
    
    /// Gets list of RoverPhoto data
    /// - Parameters:
    ///   - sol: sol for which to get the photos
    ///   - page: page for which to get the photos
    ///   - completion: completion handler containing data of the response
    static func getPhotos(sol: Int, page: Int, completion: @escaping (Result<Data>) -> Void) -> DataRequest? {
        print("fetching sol: \(sol), page: \(page)")
        guard let apiKey = self.apiKey else {
            completion(.failure(MissingAPIKeyError()))
            return nil
        }
        
        // Setup request
        let url = self.urlBase + "/mars-photos/api/v1/rovers/curiosity/photos"
        let params: Parameters = ["api_key": apiKey, "sol": sol, "page": page]
        let request = Alamofire.request(url, parameters: params)
        
        // Execute request
        request.validate().responseData { (response) in
            switch response.result {
            case .success(let value):
                completion(.success(value))

            case .failure(let error):
                completion(.failure(error))
            }
        }
        return request
    }
    
    /// Gets Data for a image given a URL
    /// - Parameters:
    ///   - url: URL of the image
    ///   - completion: completion handler containing impage data
    static func getImageFor(url: URL, completion: @escaping(Result<Data>) -> Void) -> DataRequest? {
        // Setup request
        let url = url
        let request = Alamofire.request(url)
        
        request.validate().responseData { (response) in
            switch response.result {
            case .success(let value):
                completion(.success(value))
            case .failure(let error):
                completion(.failure(error))
            }
        }
        return request
    }
}
