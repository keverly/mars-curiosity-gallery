//
//  PhotoInfoCell.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/23/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import UIKit

class PhotoInfoCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp(title: String, detail: String) {
        let titleLabel = UILabel(frame: .zero)
        titleLabel.text = title
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(titleLabel)
        
        var constraints = [
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ]
        
        let detailLabel = UILabel(frame: .zero)
        detailLabel.text = detail
        detailLabel.translatesAutoresizingMaskIntoConstraints = false
        detailLabel.textAlignment = .right
        self.contentView.addSubview(detailLabel)
        
        constraints += [
            detailLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            detailLabel.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            detailLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            detailLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    override func prepareForReuse() {
        let _ = self.contentView.subviews.map { $0.removeFromSuperview()}
    }
    
    static var stringClass: String {
        return "\(self)"
    }
}
