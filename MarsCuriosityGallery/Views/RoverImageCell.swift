//
//  RoverImageCell.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/19/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import UIKit

class RoverImageCell: UICollectionViewCell {
    
    var image: UIImage? {
        didSet {
            let imageView = UIImageView(image: image)
            imageView.contentMode = .scaleAspectFit
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.clipsToBounds = true
            self.contentView.addSubview(imageView)
                        
            var constraints = [NSLayoutConstraint]()
            
            constraints += [
                imageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
                imageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
                imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
                imageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor)
            ]
            
            NSLayoutConstraint.activate(constraints)
        }
    }
    
    // This is used so we can see the cell's row number
    var index: Int? {
        didSet {
            let labelView = UILabel(frame: .zero)
            labelView.text = String(index!)
            labelView.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addSubview(labelView)
            
            var constraints = [NSLayoutConstraint]()
            
            constraints += [
                labelView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
                labelView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
                labelView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
                labelView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor)
            ]
            
            NSLayoutConstraint.activate(constraints)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.contentView.subviews.forEach({ $0.removeFromSuperview() })
    }
    
    static var stringClass: String {
        return "\(self)"
    }
}
