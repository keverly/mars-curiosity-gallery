//
//  Bundle.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/18/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import Foundation


extension Bundle {
    /// Grabs values from info.plist
    /// - Parameter key: The key for the item in info.plist
    func infoForKey(_ key: String) -> String? {
        let info = (self.infoDictionary?[key] as? String)
        return info?.replacingOccurrences(of: "\\", with: "")
    }
}
