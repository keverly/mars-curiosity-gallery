//
//  DateFormatterExt.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/18/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import Foundation

extension DateFormatter {
    static let yyyy_MM_dd: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone.current
        formatter.locale = Locale.current
        return formatter
    }()
}
