//
//  JSONDecoderExt.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/18/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import Foundation

extension JSONDecoder {
    static let NasaAPIDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.yyyy_MM_dd)
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
}
