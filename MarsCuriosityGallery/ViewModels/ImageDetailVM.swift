//
//  ImageDetailVM.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/23/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import Foundation

class ImageDetailVM {

    private let roverPhoto: RoverPhoto!
    
    init(_ roverPhoto: RoverPhoto) {
        self.roverPhoto = roverPhoto
    }
    
    func fetchPhoto(completion: @escaping (_ data: Data?) -> Void) {
        guard let url = URL(string: self.roverPhoto.imgSrc) else { return }
        
        let _ = NasaAPI.getImageFor(url: url) { (result) in
            if let data = result.value {
                completion(data)
            }
        }
    }
    
    var roverPhotoInfo: [(String, String)] {
        roverPhoto.infoDisplay
    }
    
    var roverPhotoCamera: [(String, String)] {
        roverPhoto.cameraDisplay
    }
    
    var roverPhotoRover: [(String, String)] {
        roverPhoto.roverDisplay
    }
}
