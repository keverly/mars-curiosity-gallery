//
//  MainGalleryVM.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/19/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import Foundation
import Alamofire


protocol MainGalleryVMDelegate: class {
    func onFetchManifestCompleted()
    func onFetchPageCompleted(with newIndexPathsToReload: [IndexPath]?)
}

// We make this a class instead of struct since we will be mutating within an escaping closure.
class MainGalleryVM {
    
    private var manifest: Manifest?
    private var activePageRequest: Request?
    private var pageCache = NSCache<NSString, RoverPage>()
    private weak var delegate: MainGalleryVMDelegate?
    
    init(delegate: MainGalleryVMDelegate) {
        self.delegate = delegate
    }
    
    var totalPhotos: Int {
        return manifest?.photos.count ?? 0
    }
    
    func totalPhotosFor(sol: Int) -> Int {
        return manifest?.photos[sol].totalPhotos ?? 0
    }
    
    /// Gets RoverPhoto object from cache. Used for when users selects an item.
    /// - Parameters:
    ///   - sol: sol (section) for item that user selected
    ///   - page: page for item that user selected
    ///   - offset: offset for item that user selected
    func photoFor(sol: Int, page: Int, offset: Int) -> RoverPhoto? {
        let cacheKey = createCacheKey(sol: sol, page: page)
        guard let roverPage = pageCache.object(forKey: cacheKey) else { return nil }
        guard offset < roverPage.photos.count else { return nil }
        
        return  roverPage.photos[offset]
        
    }
    
    /// Grabs the manifest data for the curiosity rover from the NASA API
    func fetchManifest() {
        let _ = NasaAPI.getManifest { [weak self] (result) in
            guard let self = self else { return }
            guard result.isSuccess else { return }
            
            // Decode data into Manifest object
            do {
                if let data = result.value {
                    let manifestDict = try JSONDecoder.NasaAPIDecoder.decode([String: Manifest].self, from: data)
                    self.manifest = manifestDict["photo_manifest"]
                    self.manifest?.updatePhotosWithEmptyValues()
                    
                    // Let delegate know we have finished getting the manifest data
                    DispatchQueue.main.async {
                        self.delegate?.onFetchManifestCompleted()
                    }
                }
            } catch {
                print(error)
            }
        }
    }
    
    
    /// Fetches a page worth of data from either the cache or NASA API
    /// - Parameters:
    ///   - sol: sol for which to grab data
    ///   - page: page from which to grab data
    func fetchPage(sol: Int, page: Int) {
        guard activePageRequest == nil else { return }
        
        // If this page already exists in pageCache, don't run API call.
        let cacheKey = self.createCacheKey(sol: sol, page: page)
        if let roverPage = pageCache.object(forKey: cacheKey) {
            let indexPaths = self.calculateIndexPathsFor(sol: sol,
                                                         page: page,
                                                         itemsInPage: roverPage.photos.count)
            // Let delegate know we have finished getting data for page.
            self.delegate?.onFetchPageCompleted(with: indexPaths)
            return
        }
        
        // Go to API and get the photos for the specified sol and page
        activePageRequest = NasaAPI.getPhotos(sol: sol, page: page) { [weak self] (result) in
            guard let self = self else { return }
            self.activePageRequest = nil
            guard result.isSuccess else { return }
            
            do {
                if let data = result.value {
                    let roverPage = try JSONDecoder.NasaAPIDecoder.decode(RoverPage.self, from: data)
                    self.pageCache.setObject(roverPage, forKey: cacheKey)
                    let indexPaths = self.calculateIndexPathsFor(sol: sol,
                                                                 page: page,
                                                                 itemsInPage: roverPage.photos.count)
                    // Let delegate know we have finished getting data for page.
                    DispatchQueue.main.async {
                        self.delegate?.onFetchPageCompleted(with: indexPaths)
                    }
                    
                }
            } catch {
                print("Error parsing data: \(error)")
            }
        }
    }
    
    /// Downloads a specific photo
    /// - Parameters:
    ///   - sol: sol from which to get the photo
    ///   - page: page from which to get the photo
    ///   - offset: index within page to get the photo
    ///   - completion: completion handler containing optional photo Data
    func fetchPhotoFor(sol: Int, page: Int, offset: Int, completion: @escaping (_ data: Data?) -> Void) {
        let key = self.createCacheKey(sol: sol, page: page)
        guard let cachePage = self.pageCache.object(forKey: key) else {completion(nil); return }
        guard cachePage.photos.count > offset else { completion(nil); return }
        
        let imgSrc = cachePage.photos[offset].imgSrc
        let imgURL = URL(string: imgSrc)
        
        guard let unwrappedImgURL = imgURL else { completion(nil); return }
        
        let _ = NasaAPI.getImageFor(url: unwrappedImgURL) { (result) in
            if let data = result.value {
                completion(data)
            }
        }
    }
    
    /// Check to see if the specified page is in the cache
    /// - Parameters:
    ///   - sol: sol for the page
    ///   - page: page number for the page
    func hasLoadedPage(sol: Int, page: Int) -> Bool {
        let loadedPage = self.pageCache.object(forKey: self.createCacheKey(sol: sol, page: page))
        return loadedPage == nil ? false : true
    }
    
    private func createCacheKey(sol: Int, page: Int) -> NSString {
        return "\(sol)-\(page)" as NSString
    }
    
    private func calculateIndexPathsFor(sol: Int, page: Int, itemsInPage: Int) -> [IndexPath] {
        let startIndex = ((page-1) * NasaAPI.maxItemsPerPage) - 1
        let endIndex = startIndex + itemsInPage
        let refreshIndexPaths = (startIndex..<endIndex).map { IndexPath(row: $0, section: sol) }
        return refreshIndexPaths
    }
}
