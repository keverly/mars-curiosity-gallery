//
//  RoverPhoto.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/17/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import Foundation

class RoverPage: Codable {
    var photos: [RoverPhoto]
}

class RoverPhoto: Codable {
    var id: Int
    var sol: Int
    var camera: RoverPhoto.Camera
    var imgSrc: String
    var earthDate: Date
    var rover: RoverPhoto.Rover
    
    struct Camera: Codable {
        var id: Int
        var name: String
        var roverId: Int
        var fullName: String
    }
    
    struct Rover: Codable {
        var id: Int
        var name: String
        var landingDate: Date
        var launchDate: Date
        var status: String
        var maxSol: Int
        var maxDate: Date
        var totalPhotos: Int
        var cameras: [RoverPhoto.Rover.Camera]
        
        struct Camera: Codable {
            var name: String
            var fullName: String
        }
    }
    
    var infoDisplay: [(String, String)] {
        [
            ("Id", String(id)),
            ("Sol", String(sol)),
            ("Earth Date", DateFormatter.yyyy_MM_dd.string(from: earthDate))
        ]
    }
    
    var cameraDisplay: [(String, String)] {
        [
            ("Id", String(camera.id)),
            ("Name", String(camera.name)),
            ("Full Name", String(camera.fullName))
        ]
    }
    
    var roverDisplay: [(String, String)] {
        [
            ("Id", String(rover.id)),
            ("Name", String(rover.name)),
            ("Landing Date", DateFormatter.yyyy_MM_dd.string(from: rover.landingDate)),
            ("Launch Date", DateFormatter.yyyy_MM_dd.string(from: rover.launchDate)),
            ("Status", rover.status),
            ("Max Sol", String(rover.maxSol)),
            ("Max Date", DateFormatter.yyyy_MM_dd.string(from: rover.maxDate)),
            ("Total Photos", String(rover.totalPhotos))
        ]
    }

}
