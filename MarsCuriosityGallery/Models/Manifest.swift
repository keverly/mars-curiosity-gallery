//
//  Manifest.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/17/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import Foundation


struct Manifest: Codable {
    var name: String
    var landingDate: Date
    var launchDate: Date
    var status: String
    var maxSol: Int
    var maxDate: Date
    var totalPhotos: Int
    var photos: [Manifest.Photos]
    
    struct Photos: Codable {
        var sol: Int
        var earthDate: Date
        var totalPhotos: Int
        var cameras: [String]
    }
    
    // The list of photos has missing sols. We want to keep a 1:1 ratio of entries to sol.
    mutating func updatePhotosWithEmptyValues() {
        // make empty list of photos. One entry for each sol
        var emptyPhotosList = (0...maxSol).map { index in
            Photos(sol: 0, earthDate: Date(), totalPhotos: 0, cameras: [])
        }
        
        // Fill items into the emptyPhotosList
        for photo in photos {
            emptyPhotosList[photo.sol] = photo
        }
        
        //Update self.photos
        self.photos = emptyPhotosList
    }
}
