//
//  AppDelegate.swift
//  MarsCuriosityGallery
//
//  Created by Kevin on 11/17/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

