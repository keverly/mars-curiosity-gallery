# Mars Curiosity Gallery

Useing NASA's API, this app displalys a collection of images from the Mars Curiosity Rover. Selecting on an image will show a larger version of it along with details. 

## Design
This app uses MVVM design patter and contains 2 Models, 2 View Models, and 2 View Controllers along with 1 Manager and some other helper classes (extensions, Views, etc.).

### View Controllers
1. **MainGalleryVC** - This is the initial view controller. It contains a collection view that displays the images and also acts as the `dataSource` and `delegate` for the colleciton view.
2. **ImageDetailVC** - When an image in `MainGalleryVC` is selected, the `ImageDetailVC` is shown and contains a `UIImageView` for our image along with a grouped `UITableView` displaying information.

### View Models
1. **MainGalleryVM** - Referenced by `MainGalleryVC`. Handles fetching and decoding data into models. Also manages caching of retrieved pages from the API.
2. **ImageDetailVM** - Referenced by `ImageDetailVC`. Handles fetching photo data and information about the photo.

### Models
1. **Manifest** - Model representing data retrieved from `/mars-photos/api/v1/manifests/curiosity`. This contains overall information about the mission so the total number of sols and photos for creating our colleciton view can be known.
2. **RoverPhoto** - Model representing data retrieved from `/mars-photos/api/v1/rovers/curiosity/photos`. 

### Managers
1. **NasaAPI** -  Handles interaction with NASA's API. Uses Alamofire to construt and execute requests then passes data to completion handlers. Functions include `getManifest()` to get manifest data, `getPhotos()` for getting a list of photos given a page and sol, and `getImageFor()`to get image data given an image URL.

## Implementation
Just some notes on the gallery implementation. 
1. To make things simple, in our collection view, each sol is treated as a section, and each photo is treated as a row. 
2. For photos, the API will only return 1 page (25 photos) at a time. The view model for `MainGalleryVC` contains a cache of these pages retrieved from the API.
3. Whenever a cell is about to be displayed, the sol and page are calculated. Once the sol and page are known, the view model checks the cache to see if it already has retrieved this data. If it hasn't, it fires off API request.
4. Once the page data is gathered (from either the cache or API response), it refreshes the cells that need the data from this page.
5. Each cell then finds it's appropriate photo from the list of 25 returned in the page, and sets it's imageView using the url in the `srcImg` property. 
