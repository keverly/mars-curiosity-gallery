//
//  RoverPhotoTests.swift
//  MarsCuriosityGalleryTests
//
//  Created by Kevin on 11/23/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import XCTest
@testable import MarsCuriosityGallery

class RoverPhotoTests: XCTestCase {

    var roverPage: RoverPage!
    
    override func setUp() {
        if let path = Bundle(for: type(of: self)).path(forResource: "RoverPage", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                self.roverPage = try JSONDecoder.NasaAPIDecoder.decode(RoverPage.self, from: data)
              } catch {
                   print("Error parsing data. Check data format or NasaAPIDecoder")
              }
        }
    }

    override func tearDown() {}
    
    // Test RoverPhoto
    func testRoverInit() {
        // We created self.roverPage in the setup. Lets check to make sure everything was created as expected.
        XCTAssertNotNil(self.roverPage)
        
        let photo0 = self.roverPage.photos[0]
        XCTAssertEqual(photo0.id, 3132)
        XCTAssertEqual(photo0.sol, 10)
        XCTAssertEqual(photo0.imgSrc,  "http://mars.jpl.nasa.gov/msl-raw-images/proj/msl/redops/ods/surface/sol/00010/soas/rdr/ccam/CR0_398380645PRCLF0030000CCAM04010L1.PNG")
        XCTAssertEqual(DateFormatter.yyyy_MM_dd.string(from: photo0.earthDate), "2012-08-16")
    }
    
    // Test RoverPhoto.Camera
    func testRoverPhotoCamerInit() {
        XCTAssertNotNil(self.roverPage)
        
        let camera0 = self.roverPage.photos[0].camera
        XCTAssertEqual(camera0.id, 23)
        XCTAssertEqual(camera0.name, "CHEMCAM")
        XCTAssertEqual(camera0.roverId, 5)
        XCTAssertEqual(camera0.fullName, "Chemistry and Camera Complex")
    }

    // Test RoverPhoto.Rover
    func testRoverPhotoRoverInit() {
        XCTAssertNotNil(self.roverPage)
        
        let rover = self.roverPage.photos[0].rover
        XCTAssertEqual(rover.id,  5)
        XCTAssertEqual(rover.name,  "Curiosity")
        XCTAssertEqual(DateFormatter.yyyy_MM_dd.string(from: rover.landingDate), "2012-08-06")
        XCTAssertEqual(DateFormatter.yyyy_MM_dd.string(from: rover.launchDate),  "2011-11-26")
        XCTAssertEqual(rover.status, "active")
        XCTAssertEqual(rover.maxSol, 2540)
        XCTAssertEqual(DateFormatter.yyyy_MM_dd.string(from: rover.maxDate), "2019-09-28")
        XCTAssertEqual(rover.totalPhotos,  366206)
    }
    
    // Test RoverPhoto.Rover.Camera
    func testRoverPhotoRoverCameraInit() {
        XCTAssertNotNil(self.roverPage)
        
        let camera0 = self.roverPage.photos[0].rover.cameras[0]
        XCTAssertEqual(camera0.name, "FHAZ")
        XCTAssertEqual(camera0.fullName, "Front Hazard Avoidance Camera")
        
        let camera1 = self.roverPage.photos[0].rover.cameras[1]
        XCTAssertEqual(camera1.name, "NAVCAM")
        XCTAssertEqual(camera1.fullName, "Navigation Camera")
    }
}
