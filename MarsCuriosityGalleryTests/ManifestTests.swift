//
//  ManifestTests.swift
//  MarsCuriosityGalleryTests
//
//  Created by Kevin on 11/23/19.
//  Copyright © 2019 BranchCut. All rights reserved.
//

import XCTest
@testable import MarsCuriosityGallery

class ManifestTests: XCTestCase {

    var manifest: Manifest!
    
    override func setUp() {
        if let path = Bundle(for: type(of: self)).path(forResource: "Manifest", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let manifestDict = try JSONDecoder.NasaAPIDecoder.decode([String: Manifest].self, from: data)
                self.manifest = manifestDict["photo_manifest"]
              } catch {
                   print("Error parsing data. Check data format or NasaAPIDecoder")
              }
        }
    }

    override func tearDown() {}

    // Test Manifest and Manifest.Photo
    func testManifestInit() {
        // We created self.manifest in the setup. Lets check to make sure everything was created as expected
        XCTAssertNotNil(self.manifest)
        
        XCTAssertEqual(self.manifest.name, "Curiosity")
        XCTAssertEqual(DateFormatter.yyyy_MM_dd.string(from: self.manifest.landingDate), "2012-08-06")
        XCTAssertEqual(DateFormatter.yyyy_MM_dd.string(from: self.manifest.launchDate ), "2011-11-26")
        XCTAssertEqual(self.manifest.status, "active")
        XCTAssertEqual(self.manifest.maxSol, 2540)
        XCTAssertEqual(DateFormatter.yyyy_MM_dd.string(from: self.manifest.maxDate), "2019-09-28")
        XCTAssertEqual(self.manifest.totalPhotos, 366206)
        
        // Test some Photos
        let photo0 = self.manifest.photos[0]
        XCTAssertEqual(photo0.sol, 0)
        XCTAssertEqual(DateFormatter.yyyy_MM_dd.string(from: photo0.earthDate), "2012-08-06")
        XCTAssertEqual(photo0.totalPhotos, 3702)
        XCTAssertEqual(photo0.cameras, ["CHEMCAM", "FHAZ", "MARDI", "RHAZ"])
        
        // Test last Photo
        let photoLast = self.manifest.photos.last!
        XCTAssertEqual(photoLast.sol, 2540)
        XCTAssertEqual(DateFormatter.yyyy_MM_dd.string(from: photoLast.earthDate), "2019-09-28")
        XCTAssertEqual(photoLast.totalPhotos, 4)
        XCTAssertEqual(photoLast.cameras, ["FHAZ", "RHAZ"])
    }
}
